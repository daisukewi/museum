#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "practica5.h"
#include "colisiones.h"
#include "camaras.h"
#include "mundo.h"
#include "imagenes.h"
#include "planos.h"

boolean bancos_verticales [N_BANCOS];
TTexturas texturas;
short n_paredes = 0;
short n_bancos = 0;
short n_cuadros = 0;
short n_pilares = 0;
short n_puntos = 0;
short n_habitaciones = 0;

//GLfloat color_paredes [] = { 0.4f, 0.5f, 0.1f };
GLfloat color_paredes [] = { 1.0f, 1.0f, 1.0f };
//GLfloat color_suelo [] = { 0.4f, 0.5f, 0.1f };
GLfloat color_suelo [] = { 1.0f, 1.0f, 1.0f };
//GLfloat color_techo [] = { 0.4f, 0.1f, 0.4f };
GLfloat color_techo [] = { 1.0f, 1.0f, 1.0f };
//GLfloat color_cuadros [] = { 0.8f, 0.0f, 0.0f };
GLfloat color_cuadros [] = { 1.0f, 1.0f, 1.0f };
GLfloat color_bancos [] = { 0.8f, 0.2f, 0.2f };
GLfloat color_pilares [] = { 0.4f, 0.5f, 0.1f };
GLfloat color_objetos [] = { 1.0f, 1.0f, 0.0f };


/* Inserta un punto en la lista de puntos */
void insertarPunto (TPuntos puntos, int x, int y) {
	puntos[n_puntos][0] = x;
	puntos[n_puntos][1] = y;
	n_puntos++;
}

/* Inserta un cuadrado entero en la lista de puntos */
void insertarCuadrado (TPuntos puntos, int x1, int y1, int x2, int y2) {
	x1 = (x1 == x2)?((y1>y2)?x1+1:x1-1):x1;
	y1 = (y1 == y2)?((x1>x2)?y1-1:y1+1):y1;
	insertarPunto (puntos, x1, y1);
	insertarPunto (puntos, x1, y2);
	insertarPunto (puntos, x2, y2);
	insertarPunto (puntos, x2, y1);
}

void insertarTexturaPared (char *nombre_tex) {
	texturas[TEX_PARED] = carga_textura (nombre_tex);
	printf (" -Insertada textura para paredes: %s %d\n", nombre_tex, texturas[TEX_PARED]);
}

void insertarTexturaSuelo (char *nombre_tex) {
	texturas[TEX_SUELO] = carga_textura (nombre_tex);
	printf (" -Insertada textura para el suelo: %s %d\n", nombre_tex, texturas[TEX_SUELO]);
}

void insertarTexturaTecho (char *nombre_tex) {
	texturas[TEX_TECHO] = carga_textura (nombre_tex);
	printf (" -Insertada textura para el techo: %s %d\n", nombre_tex, texturas[TEX_TECHO]);
}

/* Inserta una pared en la lista de paredes */
void insertarPared (TMapa mapa_coli, TParedes paredes, int x1, int y1, int x2, int y2) {
	paredes[n_paredes][0] = (float)x1+DIST_COLI;
	paredes[n_paredes][1] = (float)y1+DIST_COLI;
	paredes[n_paredes][2] = (float)x2+DIST_COLI;
	paredes[n_paredes][3] = (float)y2+DIST_COLI;
	n_paredes++;
	insertarColision (mapa_coli, x1, y1, x2, y2);
}

void insertarTexturaBanco (char *nombre_tex) {
	texturas[TEX_BANCO] = carga_textura (nombre_tex);
	printf (" -Insertada textura para bancos: %s %d\n", nombre_tex, texturas[TEX_BANCO]);
}

/* Inserta un banco en la lista de bancos, si eta en posicion vertical o horizontal lo anota */
void insertarBanco (TMapa mapa_coli, TBancos bancos, TPuntos puntos, int x1, int y1, int x2, int y2, boolean es_vertical) {
	bancos[n_bancos][0] = (float)x1+DIST_COLI;
	bancos[n_bancos][1] = (float)y1+DIST_COLI;
	bancos[n_bancos][2] = (float)x2+DIST_COLI;
	bancos[n_bancos][3] = (float)y2+DIST_COLI;
	bancos_verticales[n_bancos++] = es_vertical;
	insertarColision (mapa_coli, x1, y1, x2, y2);
	insertarCuadrado (puntos, x1, y1, x2, y2);
}

void insertarTexturaCuadro (char *nombre_tex) {
	texturas[TEX_CUADROS+n_cuadros] = carga_textura (nombre_tex);
	printf (" -Insertada textura para el cuadro %d: %s %d\n", n_cuadros, nombre_tex, texturas[TEX_CUADROS+n_cuadros]);
}

/* Inserta un cuadro en la lista de cuadros */
void insertarCuadro (TMapa mapa_coli, TCuadros cuadros, TPuntos puntos, int x1, int y1, int z1, int x2, int y2, int z2) {
	cuadros[n_cuadros][0] = (float)x1+DIST_COLI;
	cuadros[n_cuadros][1] = (float)y1+DIST_COLI;
	cuadros[n_cuadros][2] = (float)z1;
	cuadros[n_cuadros][3] = (float)x2+DIST_COLI;
	cuadros[n_cuadros][4] = (float)y2+DIST_COLI;
	cuadros[n_cuadros][5] = (float)z2;
	n_cuadros++;
	insertarColision (mapa_coli, x1, y1, x2, y2);
	insertarCuadrado (puntos, x1, y1, x2, y2);
}

void insertarTexturaPilar (char *nombre_tex) {
	texturas[TEX_PILAR] = carga_textura (nombre_tex);
	printf (" -Insertada textura para pilares: %s %d\n", nombre_tex, texturas[TEX_PILAR]);
}

/* Inserta un pilar en la lista de pilares */
void insertarPilar (TMapa mapa_coli, TPilares pilares, TPuntos puntos, int x1, int y1, int x2, int y2, int z) {
	pilares[n_pilares][0] = (float)x1+DIST_COLI;
	pilares[n_pilares][1] = (float)y1+DIST_COLI;
	pilares[n_pilares][2] = (float)x2+DIST_COLI;
	pilares[n_pilares][3] = (float)y2+DIST_COLI;
	pilares[n_pilares][4] = (float)z;
	n_pilares++;
	insertarColision (mapa_coli, x1, y1, x2, y2);
	insertarCuadrado (puntos, x1, y1, x2, y2);
}


/* LISTAS DE VISUALIZACION */

/* Crea la lista de visualizacion de las paredes */
void creaListaParedes ( TParedes paredes ) {
	TPared pared_aux;
	char i;

	glNewList(DL_PAREDES, GL_COMPILE);
		glEnable (GL_TEXTURE_2D);
		/* Con esta funci�n indicamos que vamos a utilizar la textura */
		glBindTexture(GL_TEXTURE_2D, texturas[TEX_PARED]);
		glBegin(GL_QUADS);
			for (i=0; i<n_paredes; i++){
				creaPlanoV (pared_aux, paredes[i][0], paredes[i][1], 0.0, paredes[i][2], paredes[i][3], LIM_Z);
				glColor3fv(color_paredes);
				glNormal3fv(pared_aux[4]);
				glTexCoord2f(0.0f, 5.0f);glVertex3fv(pared_aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(pared_aux[1]);
				glTexCoord2f(5.0f, 0.0f);glVertex3fv(pared_aux[2]);
				glTexCoord2f(5.0f, 5.0f);glVertex3fv(pared_aux[3]);
			}
		glEnd();
		glDisable (GL_TEXTURE_2D);
	glEndList();
}

/* Crea la lista de visualizacion del techo y el suelo */
void creaSueloTecho ( void ) {
	TPared suelo_aux;
	glNewList(DL_SUELO, GL_COMPILE);
		glEnable (GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texturas[TEX_SUELO]);
		glBegin(GL_QUADS);
			glColor3fv(color_suelo);
			creaPlanoH(suelo_aux, 0.0, 0.0, LIM_X, LIM_Y, 0.0);
			glColor3fv(color_paredes);
			glNormal3fv(suelo_aux[4]);
			glTexCoord2f(0.0f, 10.0f);glVertex3fv(suelo_aux[0]);
			glTexCoord2f(0.0f, 0.0f);glVertex3fv(suelo_aux[1]);
			glTexCoord2f(10.0f, 0.0f);glVertex3fv(suelo_aux[2]);
			glTexCoord2f(10.0f, 10.0f);glVertex3fv(suelo_aux[3]);
		glEnd();
		glBindTexture(GL_TEXTURE_2D, texturas[TEX_TECHO]);
		glBegin(GL_QUADS);
			glColor3fv(color_techo);
			creaPlanoH(suelo_aux, 0.0, 0.0, LIM_X, LIM_Y, LIM_Z);
			glColor3fv(color_paredes);
			glNormal3fv(suelo_aux[4]);
			glTexCoord2f(0.0f, 1.0f);glVertex3fv(suelo_aux[0]);
			glTexCoord2f(0.0f, 0.0f);glVertex3fv(suelo_aux[1]);
			glTexCoord2f(1.0f, 0.0f);glVertex3fv(suelo_aux[2]);
			glTexCoord2f(1.0f, 1.0f);glVertex3fv(suelo_aux[3]);
		glEnd();
		glDisable (GL_TEXTURE_2D);
	glEndList();
}

/* Crea la lista de visualizacion de los bancos */
void creaListaBancos ( TBancos bancos ) {
	TPared aux;
	char i, j;
	
	glNewList (DL_BANCOS, GL_COMPILE);
		glEnable (GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texturas[TEX_BANCO]);
		glBegin (GL_QUADS);
			glColor3fv(color_bancos);
			for (i=0; i<n_bancos; i++){
				// Parte 1
				if (bancos_verticales[i])
					creaPlanoV(aux, bancos[i][0], bancos[i][1], 0.0, bancos[i][0]+BANCO_ANCHO_PATA, bancos[i][1], BANCO_ALTURA_MAX);
				else
					creaPlanoV(aux, bancos[i][0], bancos[i][1], 0.0, bancos[i][0], bancos[i][1]+BANCO_ANCHO_PATA, BANCO_ALTURA_MAX);
				glNormal3fv(aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(aux[3]);
				// Parte 2
				if (bancos_verticales[i])
					creaPlanoV(aux, bancos[i][0]+BANCO_ANCHO_PATA, bancos[i][1], BANCO_ALTURA_MIN, bancos[i][2]-BANCO_ANCHO_PATA, bancos[i][1], BANCO_ALTURA_MAX);
				else
					creaPlanoV(aux, bancos[i][0], bancos[i][1]+BANCO_ANCHO_PATA, BANCO_ALTURA_MIN, bancos[i][0], bancos[i][3]-BANCO_ANCHO_PATA, BANCO_ALTURA_MAX);
				glNormal3fv(aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(aux[3]);
				// Parte 3
				if (bancos_verticales[i])
					creaPlanoV(aux, bancos[i][2]-BANCO_ANCHO_PATA, bancos[i][1], 0.0, bancos[i][2], bancos[i][1], BANCO_ALTURA_MAX);
				else
					creaPlanoV(aux, bancos[i][0], bancos[i][3]-BANCO_ANCHO_PATA, 0.0, bancos[i][0], bancos[i][3], BANCO_ALTURA_MAX);
				glNormal3fv(aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(aux[3]);
				//Parte 4
				if (bancos_verticales[i])
					creaPlanoV(aux, bancos[i][2], bancos[i][1], 0.0, bancos[i][2], bancos[i][3], BANCO_ALTURA_MAX);
				else
					creaPlanoV(aux, bancos[i][0], bancos[i][3], 0.0, bancos[i][2], bancos[i][3], BANCO_ALTURA_MAX);
				glNormal3fv(aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(aux[3]);
				// Parte 5
				if (bancos_verticales[i])
					creaPlanoV(aux, bancos[i][2], bancos[i][3], 0.0, bancos[i][2]-BANCO_ANCHO_PATA, bancos[i][3], BANCO_ALTURA_MAX);
				else
					creaPlanoV(aux, bancos[i][2], bancos[i][3], 0.0, bancos[i][2], bancos[i][3]-BANCO_ANCHO_PATA, BANCO_ALTURA_MAX);
				glNormal3fv(aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(aux[3]);
				// Parte 6
				if (bancos_verticales[i])
					creaPlanoV(aux, bancos[i][2]-BANCO_ANCHO_PATA, bancos[i][3], BANCO_ALTURA_MIN, bancos[i][0]+BANCO_ANCHO_PATA, bancos[i][3], BANCO_ALTURA_MAX);
				else
					creaPlanoV(aux, bancos[i][2], bancos[i][3]-BANCO_ANCHO_PATA, BANCO_ALTURA_MIN, bancos[i][2], bancos[i][1]+BANCO_ANCHO_PATA, BANCO_ALTURA_MAX);
				glNormal3fv(aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(aux[3]);
				// Parte 7
				if (bancos_verticales[i])
					creaPlanoV(aux, bancos[i][0]+BANCO_ANCHO_PATA, bancos[i][3], 0.0, bancos[i][0], bancos[i][3], BANCO_ALTURA_MAX);
				else
					creaPlanoV(aux, bancos[i][2], bancos[i][1]+BANCO_ANCHO_PATA, 0.0, bancos[i][2], bancos[i][1], BANCO_ALTURA_MAX);
				glNormal3fv(aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(aux[3]);
				// Parte 8
				if (bancos_verticales[i])
					creaPlanoV(aux, bancos[i][0], bancos[i][3], 0.0, bancos[i][0], bancos[i][1], BANCO_ALTURA_MAX);
				else
					creaPlanoV(aux, bancos[i][2], bancos[i][1], 0.0, bancos[i][0], bancos[i][1], BANCO_ALTURA_MAX);
				glNormal3fv(aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(aux[3]);
				// Parte 9
				if (bancos_verticales[i])
					creaPlanoH(aux, bancos[i][0], bancos[i][1], bancos[i][2], bancos[i][3], BANCO_ALTURA_MAX);
				else
					creaPlanoH(aux, bancos[i][0], bancos[i][3], bancos[i][2], bancos[i][1], BANCO_ALTURA_MAX);
				glNormal3fv(aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(aux[3]);
			}
		glEnd();
		glDisable (GL_TEXTURE_2D);
	glEndList();
}

/* Crea la lista de visualizacion de los cuadros */
void creaListaCuadros ( TCuadros cuadros ) {
	TPared cuadro_aux;
	char i;

	glNewList(DL_CUADROS, GL_COMPILE);
		glEnable (GL_TEXTURE_2D);
		glPushMatrix();
		for (i=0; i<n_cuadros; i++){
			glBindTexture(GL_TEXTURE_2D, texturas[TEX_CUADROS+i]);
			glBegin(GL_QUADS);
				creaPlanoV(cuadro_aux, cuadros[i][0], cuadros[i][1], cuadros[i][2], cuadros[i][3], cuadros[i][4], cuadros[i][5]);
				glColor3fv(color_cuadros);
				glNormal3fv(cuadro_aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(cuadro_aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(cuadro_aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(cuadro_aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(cuadro_aux[3]);
			glEnd();
		}
		glPopMatrix();
		glDisable (GL_TEXTURE_2D);
	glEndList();
}

/* Crea la lista de visualizacion de los pilares */
void creaListaPilares ( TPilares pilares ) {
	TPared pared_aux;
	char i;

	glNewList(DL_PILARES, GL_COMPILE);
		glBindTexture(GL_TEXTURE_2D, texturas[TEX_PILAR]);
		for (i=0; i<n_pilares; i++){
			glEnable (GL_TEXTURE_2D);
			glColor3fv(color_pilares);
			glBegin(GL_QUADS);
				creaPlanoV(pared_aux, pilares[i][0], pilares[i][1], 0, pilares[i][2], pilares[i][1], pilares[i][4]);
				glNormal3fv(pared_aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(pared_aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(pared_aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(pared_aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(pared_aux[3]);
				creaPlanoV(pared_aux, pilares[i][2], pilares[i][1], 0, pilares[i][2], pilares[i][3], pilares[i][4]);
				glNormal3fv(pared_aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(pared_aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(pared_aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(pared_aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(pared_aux[3]);
				creaPlanoV(pared_aux, pilares[i][2], pilares[i][3], 0, pilares[i][0], pilares[i][3], pilares[i][4]);
				glNormal3fv(pared_aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(pared_aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(pared_aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(pared_aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(pared_aux[3]);
				creaPlanoV(pared_aux, pilares[i][0], pilares[i][3], 0, pilares[i][0], pilares[i][1], pilares[i][4]);
				glNormal3fv(pared_aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(pared_aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(pared_aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(pared_aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(pared_aux[3]);
				creaPlanoH(pared_aux, pilares[i][0], pilares[i][1], pilares[i][2], pilares[i][3], pilares[i][4]);
				glNormal3fv(pared_aux[4]);
				glTexCoord2f(0.0f, 1.0f);glVertex3fv(pared_aux[0]);
				glTexCoord2f(0.0f, 0.0f);glVertex3fv(pared_aux[1]);
				glTexCoord2f(1.0f, 0.0f);glVertex3fv(pared_aux[2]);
				glTexCoord2f(1.0f, 1.0f);glVertex3fv(pared_aux[3]);
			glEnd();
			glDisable (GL_TEXTURE_2D);
			glColor3fv(color_objetos);
			// En cada pilar pone alternativamente una esfera o un cono invertido.
			if (fmod(i,2)) {
				glPushMatrix();
					glTranslatef (pilares[i][0]+(pilares[i][2]-pilares[i][0])/2, pilares[i][1]+(pilares[i][3]-pilares[i][1])/2, pilares[i][4]+1.0);
					glutSolidSphere (1, CARAS_CIRCULO, CARAS_CIRCULO);
				glPopMatrix();
			} else {
				glPushMatrix();
					glTranslatef (pilares[i][0]+(pilares[i][2]-pilares[i][0])/2, pilares[i][1]+(pilares[i][3]-pilares[i][1])/2, pilares[i][4]);
					creaSolidInvertedCone (3.0, CARAS_CIRCULO, 1, pilares[i][3]-pilares[i][1]);
				glPopMatrix();
			}
		}
	glEndList();
}

void pintarEscenario ( void ) {
	glCallList(DL_SUELO); //pinta el suelo y el techo
	glCallList(DL_PAREDES); //pinta las paredes
	glCallList(DL_BANCOS);	//pinta los bancos
	glCallList(DL_PILARES); //Pinta los pilares
	glCallList(DL_CUADROS); //Pinta los cuadros
}
