#include <GL/glut.h>
#include "practica5.h"
#include "planos.h"

/* Devuelve el vector normal a los dos dados como p y q */
float *vecNormal ( float p[3], float q[3], float n[3] ) {
	float nmod;

	n[0] = p[1]*q[2] - p[2]*q[1];
	n[1] = -p[0]*q[2] + p[2]*q[0];
	n[2] = p[0]*q[1] - p[1]*q[0];
	nmod = sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);
	n[0] /= nmod;
	n[1] /= nmod;
	n[2] /= nmod;

	return n;
}

/* Dado un plano, devuelve su vector normal correspondiente */
float *normalizar ( float v[5][3] ) {
	float p[3], q[3];

	p[0] = v[0][0]-v[1][0];
	p[1] = v[0][1]-v[1][1];
	p[2] = v[0][2]-v[1][2];
	q[0] = v[0][0]-v[2][0];
	q[1] = v[0][1]-v[2][1];
	q[2] = v[0][2]-v[2][2];

	return vecNormal(p,q,v[4]);
}

// Se pasan los puntos de la esquina inferior-izquierda y la esquina superior-derecha y crea el plano vertical en aux
void creaPlanoV ( float aux [5][3], float x1, float y1, float z1, float x2, float y2, float z2 ) {
	aux[0][0] = x1; aux[0][1] = y1; aux[0][2] = z2;
	aux[1][0] = x1; aux[1][1] = y1; aux[1][2] = z1;
	aux[2][0] = x2; aux[2][1] = y2; aux[2][2] = z1;
	aux[3][0] = x2; aux[3][1] = y2; aux[3][2] = z2;
	normalizar (aux);
}

// Se pasan los puntos de una esquina y de la contraria y crea el plano horizontal en aux
void creaPlanoH ( float aux [5][3], float x1, float y1, float x2, float y2, float z ) {
	if (z < LIM_Z){
		aux[0][0] = x1; aux[0][1] = y1; aux[0][2] = z;
		aux[1][0] = x2; aux[1][1] = y1; aux[1][2] = z;
		aux[2][0] = x2; aux[2][1] = y2; aux[2][2] = z;
		aux[3][0] = x1; aux[3][1] = y2; aux[3][2] = z;
	} else {
		aux[0][0] = x1; aux[0][1] = y1; aux[0][2] = z;
		aux[1][0] = x1; aux[1][1] = y2; aux[1][2] = z;
		aux[2][0] = x2; aux[2][1] = y2; aux[2][2] = z;
		aux[3][0] = x2; aux[3][1] = y1; aux[3][2] = z;
	}
	normalizar (aux);
}

/* Crea un circulo en la posicion x, y, z con el radio r */
void crearCirculo (float x, float y, float z, float r) {
	int i;
	glBegin(GL_POLYGON); 
		glNormal3f( 0.0, 0.0, 1.0);
     for (i = 0; i < CARAS_CIRCULO + 1; i++) {  // +1 para cerrar 
         glVertex3f( x + r * cos(2.0 * PI * i / CARAS_CIRCULO), y + r * sin(2.0 * PI * i / CARAS_CIRCULO), z ); 
    } 
	glEnd();
}

void creaSolidInvertedCone (GLdouble altura, GLint slices, GLint stacks, float radio) {
	GLUquadric *cil; // define puntero a una cuádrica
	
	cil = gluNewQuadric(); // crea la cuádrica asociada
	gluQuadricDrawStyle (cil, GLU_FILL); // crea el estilo de relleno
	gluCylinder (cil, 0.0, 1.0, altura, slices, stacks); // crea el cono con sus parámetros
	crearCirculo (0, 0, altura, radio);
}
