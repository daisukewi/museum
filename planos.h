#ifndef _PLANOS_
#define _PLANOS_

float *vecNormal ( float p[3], float q[3], float n[3] );
float *normalizar ( float v[5][3] );
void creaPlanoV ( float aux [5][3], float x1, float y1, float z1, float x2, float y2, float z2 );
void creaPlanoH ( float aux [5][3], float x1, float y1, float x2, float y2, float z );
void crearCirculo (float x, float y, float z, float r);
void creaSolidInvertedCone (GLdouble altura, GLint slices, GLint stacks, float radio);

#endif
