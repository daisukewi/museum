#ifndef _COLIS_
#define _COLIS_

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))

#define NO_COLI		0
#define CUADRO_1	6
#define CUADRO_2	7
#define CUADRO_3	8
#define CUADRO_4	9
#define CUADRO_5	10
#define CUADRO_6	11
#define CUADRO_7	12
#define CUADRO_8	13
#define CUADRO_9	14
#define CUADRO_10	15
#define ENTRADA		20
#define COLI			255

typedef unsigned char TColision;
typedef TColision TMapa  [LIM_X][LIM_Y];

#endif
