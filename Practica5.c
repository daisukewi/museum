/*******************************************/
/*                                         */
/*   E N T O R N O S   V I R T U A L E S   */
/*                                         */
/*           P r a c t i c a   5           */
/*                                         */
/*******************************************/

#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>
#include "practica5.h"
#include "colisiones.h"
#include "camaras.h"
#include "mundo.h"

GLfloat difusa_encendida[] = { 0.7f, 0.7f, 0.7f, 1.0f}; // luz blanca
GLfloat difusa_apagada[] = { 0.1f, 0.1f, 0.1f, 1.0f}; // luz negra
GLfloat mat_ambient[] = {0.7f, 0.7f, 0.7f, 1.0f}; //gris
GLfloat mat_diffuse[] = {0.6f, 0.6f, 0.6f, 1.0f};
GLfloat mat_specular[] = {0.1f, 0.1f, 0.1f, 1.0f};
GLfloat mat_shininess[]= {50.0f};
GLfloat light0_pos[] = { 15.0f, 15.0f, 8.0f, 1.0f }; // posici�n de la luz 0
GLfloat light1_pos[] = { 40.0f, 70.0f, 8.0f, 1.0f }; // posici�n de la luz 1
GLfloat light2_pos[] = { 15.0f, 125.0f, 8.0f, 1.0f }; // posici�n de la luz 3
GLfloat light0_mano[] = { 0.0f, 0.0f, 0.0f, 1.0f }; // posici�n de la luz 0

GLuint lista_paredes;  //Lista de visualizacion de las paredes
TMapa mapa_colisiones; //estructura de datos para manejar las colisiones

GLuint next_time = 0;
int ancho, alto;
int mover_raton = 1;
int vista_actual;
TColision posicion_jugador;
TKeyState estado_personaje;
TCamaras camaras;	//Crea un array para guardar las diferentes camaras
short n_camaras = N_CAMARAS;

void pintarLuces ( void ) {
	glDisable (GL_TEXTURE_2D);
	glPushMatrix();
		glColor3f (1.0, 1.0, 0.0);
		glTranslatef (light0_pos[0], light0_pos[1], 10);
		glutSolidSphere (1, CARAS_CIRCULO, CARAS_CIRCULO);
	glPopMatrix();
	glPushMatrix();
		glTranslatef (light1_pos[0], light1_pos[1], 10);
		glutSolidSphere (1, CARAS_CIRCULO, CARAS_CIRCULO);
	glPopMatrix();
	glPushMatrix();
		glTranslatef (light2_pos[0], light2_pos[1], 10);
		glutSolidSphere (1, CARAS_CIRCULO, CARAS_CIRCULO);
	glPopMatrix();
}

void iniCamaras () {
	int i;
	vista_actual = JUGADOR;
	for (i=0; i<N_ESTADOS; i++) estado_personaje[i] = FALSE;
		printf ("  -Camara Jugador\n");
	iniPosicion (&camaras[JUGADOR], X_INI, Y_INI, ALTURA);	// Mover el observador a la posicion inicial
	iniVista (&camaras[JUGADOR], ALFA_INI, BETA_INI);				// Mover la vista a (0,0)
		printf ("  -Camara 1\n");
	iniPosicion (&camaras[CAMARA_1], X_CAM_1, Y_CAM_1, Z_CAM_1);	// Mover las camaras a su posicion inicial
	iniVista (&camaras[CAMARA_1], ALFA_CAM_1, BETA_CAM_1);
		printf ("  -Camara 2\n");
	iniPosicion (&camaras[CAMARA_2], X_CAM_2, Y_CAM_2, Z_CAM_2);
	iniVista (&camaras[CAMARA_2], ALFA_CAM_2, BETA_CAM_2);
		printf ("  -Camara 3\n");
	iniPosicion (&camaras[CAMARA_3], X_CAM_3, Y_CAM_3, Z_CAM_3);
	iniVista (&camaras[CAMARA_3], ALFA_CAM_3, BETA_CAM_3);
		printf ("  -Camara 4\n");
	iniPosicion (&camaras[CAMARA_4], X_CAM_4, Y_CAM_4, Z_CAM_4);
	iniVista (&camaras[CAMARA_4], ALFA_CAM_4, BETA_CAM_4);
		printf ("  -Camara Giratoria\n");
	iniPosicion (&camaras[CAMARA_G], X_CAM_G, Y_CAM_G, Z_CAM_G);
	iniVista (&camaras[CAMARA_G], ALFA_CAM_G, BETA_CAM_G);

	glutWarpPointer(ancho/2,alto/2);
}

void iniLuces ( void ) {
	glLightfv(GL_LIGHT0, GL_DIFFUSE, difusa_encendida); // Se asignan los par�metros a la luz 0
	glEnable(GL_LIGHT0); // Se enciende la luz 0
	
	glLightfv(GL_LIGHT1, GL_DIFFUSE, difusa_encendida); // Se asignan los par�metros a la luz 1
	glEnable(GL_LIGHT1); // Se enciende la luz 1
	
	glLightfv(GL_LIGHT2, GL_DIFFUSE, difusa_encendida); // Se asignan los par�metros a la luz 2
	glEnable(GL_LIGHT2); // Se enciende la luz 2

	glEnable(GL_LIGHTING); // Se activan los c�lculos de la iluminaci�n
	glShadeModel(GL_SMOOTH); // Se define el modelo suave

	glEnable(GL_COLOR_MATERIAL);  //activar color
	glColorMaterial(GL_FRONT, GL_DIFFUSE);  //caras del objeto afectadas por el material
	glMaterialfv(GL_FRONT,GL_AMBIENT,mat_ambient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,mat_diffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,mat_specular);
	glMaterialfv(GL_FRONT,GL_SHININESS,mat_shininess);

	//glEnable(GL_TEXTURE_2D);	//activa la utilizacion de texturas <-- Aqui no se inicializa esto
	glEnable(GL_DEPTH_TEST);  //activar ocultacion de caras
}

/* Funcion para inicializar algunos parametros de OpenGL */
void init ( void ) {
	glClearColor(0.0,0.0,0.0,0.0);
		printf ("Cargando camaras...\n");
	iniCamaras ();
	cargarMundo ( mapa_colisiones );
		printf ("Cargando luces...\n");
	iniLuces ();
	posicion_jugador = mover (&camaras[JUGADOR], 0.0, 0.0);
}

/* Funcion que se llamara cada vez que se redimensione la ventana */
void reshape ( int w , int h ) {
	glViewport ( 0, 0, w, h ) ;											/* Se establece el marco del dispositivo */
	glMatrixMode ( GL_PROJECTION ) ;                 /* Activa la matriz de proyecci�n */
	glLoadIdentity ( ) ;                             /* Se carga la matriz identidad */ 
	gluPerspective (45.0, w/(float)h, 0.1, 1000.0 ) ;/* Se establece el tipo de proyecci�n */
	glMatrixMode ( GL_MODELVIEW ) ;                  /* Activa la matriz de modelo-vista */
	glLoadIdentity ( ) ;                             /* Se carga la matriz identidad */
	ancho = w; alto = h;

	cargaHUD (ancho, alto);
	cargaMapa (ancho, alto);
}

/* Funcion que se llamara cada vez que se dibuje en pantalla */
void display ( void ) {
	glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) ;

	glMatrixMode ( GL_MODELVIEW ) ;
	glLoadIdentity ( ) ;
	//glLightfv(GL_LIGHT0, GL_POSITION, light0_mano);
	ponerVista ( &camaras[vista_actual] ) ;

	/* Luces */
	glLightfv(GL_LIGHT0, GL_POSITION, light0_pos);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_pos);
	glLightfv(GL_LIGHT2, GL_POSITION, light2_pos);
	/* /Luces */

	//pintar escenario
	pintarEscenario ();
	pintarLuces ();
	
	if (estado_personaje[VISTA_MAPA])
		pintarMapa (&camaras[JUGADOR], ancho, alto);
	else if ((posicion_jugador != NO_COLI) && (vista_actual == JUGADOR))
		imprimeHUD (ancho, alto, posicion_jugador);
	else if (vista_actual != JUGADOR)
		imprimeHUD (ancho, alto, vista_actual);

	glutSwapBuffers ( ) ;
}

/* Funcion que controla los eventos de teclado */
void keyboard ( unsigned char key, int x, int y ) {
	//estado_personaje[AGACHADO] = glutGetModifiers () == GLUT_ACTIVE_CTRL;
	switch ( key ) {
		case 27:  exit ( 0 ) ; /* tecla escape*/
							break ;
		case 'f': glutFullScreen() ;
							break ;
		case 'v': glutReshapeWindow ( WIDTH, HEIGHT ) ;
							break ;
		case 'p': //imprimirPantalla (ancho, alto);
							break;
		case 'h': printf ("alfa: %f n\n", camaras[vista_actual].alfa/PI);
							printf ("beta: %f n\n\n", camaras[vista_actual].beta/PI);
							printf ("eyex: %f\n", camaras[vista_actual].eyex);
							printf ("eyey: %f\n", camaras[vista_actual].eyey);
							printf ("eyez: %f\n\n", camaras[vista_actual].eyez);
							printf ("atx: %f\n", camaras[vista_actual].atx);
							printf ("aty: %f\n", camaras[vista_actual].aty);
							printf ("atz: %f\n", camaras[vista_actual].atz);
							printf ("---------------\n\n");
							imprimirMapa(mapa_colisiones, &camaras[JUGADOR]);
							break ;
		case 'c': vista_actual = fmod (vista_actual + 1, n_camaras);
							glutWarpPointer(ancho/2,alto/2);
							break ;
		case 9: 
#ifndef MI_GLUT
							estado_personaje[VISTA_MAPA] = fmod (estado_personaje[VISTA_MAPA]+1, 2);
							glutWarpPointer(ancho/2,alto/2);
#else
							estado_personaje[VISTA_MAPA] = TRUE;
#endif
							break ;
		case '1': if (posicion_jugador == ENTRADA) {
								glLightfv(GL_LIGHT0, GL_DIFFUSE, difusa_apagada);
								glLightfv(GL_LIGHT1, GL_DIFFUSE, difusa_apagada);
								glLightfv(GL_LIGHT2, GL_DIFFUSE, difusa_apagada);
							}
							break ;
		case '2': if (posicion_jugador == ENTRADA) {
								glLightfv(GL_LIGHT0, GL_DIFFUSE, difusa_encendida);
								glLightfv(GL_LIGHT1, GL_DIFFUSE, difusa_encendida);
								glLightfv(GL_LIGHT2, GL_DIFFUSE, difusa_encendida);
							}
							break ;
		case 'w': estado_personaje[AVANCE] = TRUE; break ;
		case 's': estado_personaje[RETROCESO] = TRUE; break ;
		case 'a': estado_personaje[IZQUIERDA] = TRUE; break ;
		case 'd': estado_personaje[DERECHA] = TRUE; break ;
		case ' ': 
#ifndef MI_GLUT
							estado_personaje[AGACHADO] = fmod (estado_personaje[AGACHADO]+1, 2);
#else
							estado_personaje[AGACHADO] = TRUE;
#endif
							break;
	}
}

void keyboardUp ( unsigned char key, int x, int y ) {
	switch ( key ) {
		case 'w': estado_personaje[AVANCE] = FALSE; break ;
		case 's': estado_personaje[RETROCESO] = FALSE; break ;
		case 'a': estado_personaje[IZQUIERDA] = FALSE; break ;
		case 'd': estado_personaje[DERECHA] = FALSE; break ;
		case ' ': estado_personaje[AGACHADO] = FALSE; break;
		case 9: estado_personaje[VISTA_MAPA] = FALSE; glutWarpPointer(ancho/2,alto/2); break;
	}
}

void especial ( int key, int x, int y ) {
	switch ( key ) {
		case GLUT_KEY_UP: estado_personaje[AVANCE] = TRUE; break;
		case GLUT_KEY_DOWN: estado_personaje[RETROCESO] = TRUE; break;
		case GLUT_KEY_LEFT: estado_personaje[GIRO_IZQ] = TRUE; break;
		case GLUT_KEY_RIGHT: estado_personaje[GIRO_DER] = TRUE; break;
	}
}

void especialUp ( int key, int x, int y ) {
	switch ( key ) {
		case GLUT_KEY_UP: estado_personaje[AVANCE] = FALSE; break;
		case GLUT_KEY_DOWN: estado_personaje[RETROCESO] = FALSE; break;
		case GLUT_KEY_LEFT: estado_personaje[GIRO_IZQ] = FALSE; break;
		case GLUT_KEY_RIGHT: estado_personaje[GIRO_DER] = FALSE; break;
	}
}

void mouse (int button, int state, int x, int y) {
	switch ( button ) {
		case GLUT_LEFT_BUTTON: estado_personaje[BOTON_IZQ] = (state == GLUT_DOWN)?TRUE:FALSE; break;
		case GLUT_RIGHT_BUTTON: estado_personaje[BOTON_DER] = (state == GLUT_DOWN)?TRUE:FALSE; break;
	}
}

void ocioso ( void ) {
	int i;
	GLuint now = glutGet(GLUT_ELAPSED_TIME);
	if (next_time <= now) {
		girar (&camaras[CAMARA_G], GIRO_CAM, 0.0);
		next_time += TICK_INTERVAL;
	}
	// Comprueba las teclas puladas por el jugador y ejecuta la accion debida
	if (vista_actual == JUGADOR
			//&& !estado_personaje[VISTA_MAPA]
			) {
		if (estado_personaje[AVANCE]) posicion_jugador = mover (&camaras[JUGADOR], VELOCIDAD, 0.0 );
		if (estado_personaje[RETROCESO]) posicion_jugador = mover (&camaras[JUGADOR], -VELOCIDAD, 0.0 );
		if (estado_personaje[IZQUIERDA]) posicion_jugador = mover (&camaras[JUGADOR], 0.0, VELOCIDAD_LATERAL );
		if (estado_personaje[DERECHA]) posicion_jugador = mover (&camaras[JUGADOR], 0.0, -VELOCIDAD_LATERAL );
		if (estado_personaje[AGACHADO]) cambiarAltura (&camaras[JUGADOR], MEDIA_ALTURA); else cambiarAltura (&camaras[JUGADOR], ALTURA);
		if (estado_personaje[GIRO_IZQ]) girar (&camaras[JUGADOR], GIRO, 0.0 );
		if (estado_personaje[GIRO_DER]) girar (&camaras[JUGADOR], -GIRO, 0.0 );
		if (estado_personaje[GIRO_UP]) girar (&camaras[JUGADOR], 0.0, GIRO );
		if (estado_personaje[GIRO_DOWN]) girar (&camaras[JUGADOR], 0.0, -GIRO );
		if (estado_personaje[BOTON_IZQ]) iniVistaVertical (&camaras[JUGADOR], BETA_INI);
		if (estado_personaje[BOTON_DER]) iniVistaHorizontal (&camaras[JUGADOR], ALFA_INI);
	}
#ifndef MI_GLUT
	for (i=0; i<N_ESTADOS-2; i++) estado_personaje[i] = FALSE;
#endif
	glutPostRedisplay();
}

void mouseMove (int x, int y) {
	float alfa, beta;
	if (mover_raton && vista_actual == JUGADOR
			//&& !estado_personaje[VISTA_MAPA]
			) {
		alfa = atan ((x - ancho/2) / PRECISION);
		beta = atan ((y - alto/2) / PRECISION);
		girar (&camaras[vista_actual], -alfa, -beta);
		glutPostRedisplay();
		mover_raton = 0;
		glutWarpPointer(ancho/2,alto/2);
	} else if (!mover_raton) {
		mover_raton = 1;
	}
}


/* Funcion principal */
int main ( int argc, char** argv ) {

		printf("Cargando...\n");
	glutInit ( &argc, argv ) ;
		printf(" * Trazando reticulas de Splines\n");
	glutInitDisplayMode ( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH ) ;
	glutInitWindowSize ( WIDTH, HEIGHT ) ;
		printf(" * Recalculando comportamiento agresivo computacional\n");
	glutInitWindowPosition ( 400, 300 );
	glutCreateWindow ( "Practica 5" ) ;
		printf(" * Incorporando barredoras inteligentes\n");
	glutFullScreen();
	glutSetCursor(GLUT_CURSOR_CROSSHAIR);

	init ( ) ;
		printf ("Cargando funciones\n");
		printf ("  -Funcion Reshape\n");
	glutReshapeFunc ( reshape ) ;
		printf ("  -Funcion Keyboard\n");
	glutKeyboardFunc ( keyboard ) ;
		printf ("  -Funcion Especial\n");
	glutSpecialFunc ( especial ) ;
#ifdef MI_GLUT
		printf ("  -Funcion Keyboard Up\n");
	glutKeyboardUpFunc ( keyboardUp ) ;
		printf ("  -Funcion Especial Up\n");
	glutSpecialUpFunc	( especialUp ) ;
#endif
		printf ("  -Funcion Mouse\n");
	glutMouseFunc ( mouse ) ;
		printf ("  -Funcion MouseMotion\n");
	glutPassiveMotionFunc ( mouseMove ) ;
		printf ("  -Funcion Display\n");
	glutDisplayFunc ( display ) ;
		printf ("  -Funcion Idle\n");
	glutIdleFunc ( ocioso ) ;
		printf ("Todo correcto iniciando bucle principal...\n");
	glutMainLoop ( ) ;

	return 0 ;
}
