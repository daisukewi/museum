#include <GL/glut.h>
#include "practica5.h"
#include "colisiones.h"
#include "camaras.h"
#include "mundo.h"

/* Funcion que se llama para girar la mirada del observador */
void girar (TCamara *obj, float a, float b ) {
	obj->alfa = fmod(obj->alfa + a, 2*PI);
	obj->beta = ((obj->beta+b > GIRO_MAX)? GIRO_MAX : obj->beta+b);
	obj->beta = ((obj->beta+b < GIRO_MIN)? GIRO_MIN : obj->beta+b);

	obj->atx = obj->eyex + DIST * cos (obj->beta) * cos (obj->alfa);
	obj->aty = obj->eyey + DIST * cos (obj->beta) * sin (obj->alfa);
	obj->atz = obj->eyez + DIST * sin (obj->beta);
}

/* Funcion que se llama para el avance y desplazamiento lateral del observador */
TColision mover (TCamara *obj, float front, float side) {
	TColision pos;
	extern TMapa mapa_colisiones;
	if (front) {
		obj->eyex += front * cos (obj->alfa);     // movimieto de avance
		obj->eyey += front * sin (obj->alfa);     // y retroceso
	}
	if (side) {     
		obj->eyex -= side * sin (obj->alfa);      // movimiento de desplazamiento
		obj->eyey += side * cos (obj->alfa);      // lateral
	}
	pos = comprobarColision (mapa_colisiones, obj);	// Comprueba que al moverse no choque con los objetos y devuelve donde esta
	girar (obj, 0.0, 0.0);
	return (pos);
}

void cambiarAltura (TCamara *obj, float nueva_altura) {
	obj->eyez = nueva_altura;
	girar (obj, 0.0, 0.0);
}

/* Funcion que inicializa el giro de una camara */
void iniVista ( TCamara *obj, float a, float b ) {
	obj->alfa = a;
	obj->beta = b;

	girar (obj, 0.0, 0.0);
}
void iniVistav ( TCamara *obj, float f[2] ) {
	iniVista (obj, f[0], f[1]);
}

/* Funcion que inicializa la posicion de una camara */
void iniPosicion ( TCamara *obj, float x, float y, float z ) {
	obj->eyex = x;
	obj->eyey = y;
	obj->eyez = z;
	obj->upx = 0.0;
	obj->upy = 0.0;
	obj->upz = 1.0;

	girar (obj, 0.0, 0.0);
}
void iniPosicionv ( TCamara *obj, float f[3] ) {
	iniPosicion (obj, f[0], f[1], f[2]);
}

void ponerVista (TCamara *obj) {
	gluLookAt (obj->eyex, obj->eyey, obj->eyez, obj->atx, obj->aty, obj->atz, obj->upx, obj->upy, obj->upz);
}
