#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include "practica5.h"
#include "colisiones.h"
#include "camaras.h"
#include "mundo.h"
//#include "imagenes.h"

TPuntos puntos;
TParedes paredes;      //Estructura de datos en la que se almacenan las paredes
TBancos bancos;
TCuadros cuadros;
TPilares pilares;

void cargaParedes ( FILE *f, TMapa mapa_coli ) {
	int i, n_aux, x1, x2, y1, y2;
	char nombre_tex[MAX_LONG_NOMBRE];
	extern short n_habitaciones;
	if (!feof(f)) { // Lee el numero de paredes que hay
		fscanf (f, "%s", &nombre_tex[0]);
		insertarTexturaPared (&nombre_tex[0]);
		//fscanf (f, "%s", &nombre_tex[0]);
		//insertarTexturaSuelo (&nombre_tex[0]);
		//fscanf (f, "%s", &nombre_tex[0]);
		//insertarTexturaTecho (&nombre_tex[0]);
		fscanf (f, "%d", &n_aux);
		for (i=0; i < n_aux; i++) {	// Carga todas las paredes
			fscanf (f, "%d %d %d %d", &x1, &y1, &x2, &y2);
			insertarPared (mapa_coli, paredes, x1, y1, x2, y2);
		}
	}
	if (!feof(f)) { // Lee el numero de habitacioness que hay
		fscanf (f, "%d", &n_aux);
		for (i=0; i < n_aux; i++) {	//Carga las habitaciones para el mini-mapa
			fscanf (f, "%d %d %d %d", &x1, &y1, &x2, &y2);
			insertarCuadrado (puntos, x1, y1, x2, y2);
		}
		n_habitaciones = n_aux;
	}
}

void iniParedes ( TMapa mapa_coli ) {
	extern short n_habitaciones;
	insertarTexturaPared (DIB_PAREDES);
	insertarTexturaSuelo (DIB_SUELO);
	insertarTexturaTecho (DIB_TECHO);
	insertarPared (mapa_coli, paredes, 0, 0, 0, 30);
	insertarPared (mapa_coli, paredes, 0, 30, 10, 30);
	insertarPared (mapa_coli, paredes, 10, 30, 10, 50);
	insertarPared (mapa_coli, paredes, 10, 50, 0, 50);
	insertarPared (mapa_coli, paredes, 0, 50, 0, 90);
	insertarPared (mapa_coli, paredes, 0, 90, 10, 90);
	insertarPared (mapa_coli, paredes, 10, 90, 10, 110);
	insertarPared (mapa_coli, paredes, 10, 110, 0, 110);
	insertarPared (mapa_coli, paredes, 0, 110, 0, 140);
	insertarPared (mapa_coli, paredes, 0, 140, 30, 140);
	insertarPared (mapa_coli, paredes, 30, 140, 30, 130);
	insertarPared (mapa_coli, paredes, 30, 130, 40, 130);
	insertarPared (mapa_coli, paredes, 40, 130, 40, 120);
	insertarPared (mapa_coli, paredes, 40, 120, 30, 120);
	insertarPared (mapa_coli, paredes, 30, 120, 30, 110);
	insertarPared (mapa_coli, paredes, 30, 110, 20, 110);
	insertarPared (mapa_coli, paredes, 20, 110, 20, 90);
	insertarPared (mapa_coli, paredes, 20, 90, 80, 90);
	insertarPared (mapa_coli, paredes, 80, 90, 80, 50);
	insertarPared (mapa_coli, paredes, 80, 50, 20, 50);
	insertarPared (mapa_coli, paredes, 20, 50, 20, 30);
	insertarPared (mapa_coli, paredes, 20, 30, 30, 30);
	insertarPared (mapa_coli, paredes, 30, 30, 30, 0);
	insertarPared (mapa_coli, paredes, 30, 0, 0, 0);

	insertarCuadrado (puntos, 0, 0, 30, 30);
	insertarCuadrado (puntos, 10, 30, 20, 50);
	insertarCuadrado (puntos, 0, 50, 80, 90);
	insertarCuadrado (puntos, 10, 90, 20, 110);
	insertarCuadrado (puntos, 0, 110, 30, 140);
	insertarCuadrado (puntos, 30, 120, 40, 130);
	n_habitaciones = 6;
}

void cargaBancos ( FILE *f, TMapa mapa_coli ) {
	int i, n_aux, x1, x2, y1, y2, z;
	char nombre_tex[MAX_LONG_NOMBRE];
	if (!feof(f)) { // Lee el numero de bancos que hay
		//fscanf (f, "%s", &nombre_tex[0]);
		//insertarTexturaBanco (&nombre_tex[0]);
		fscanf (f, "%d", &n_aux);
		for (i=0; i < n_aux; i++) {	// Carga todos los bancos
			fscanf (f, "%d %d %d %d %d", &x1, &y1, &x2, &y2, &z);
			insertarBanco (mapa_coli, bancos, puntos, x1, y1, x2, y2, (unsigned char)z);
		}
	}
}

void iniBancos ( TMapa mapa_coli ) {
	insertarTexturaBanco (DIB_BANCO);
	insertarBanco (mapa_coli, bancos, puntos, 27, 55, 37, 58, TRUE);
	insertarBanco (mapa_coli, bancos, puntos, 75, 65, 72, 75, FALSE);
	insertarBanco (mapa_coli, bancos, puntos, 50, 55, 65, 58, TRUE);
	insertarBanco (mapa_coli, bancos, puntos, 27, 82, 37, 85, TRUE);
	insertarBanco (mapa_coli, bancos, puntos, 50, 82, 65, 85, TRUE);
	insertarBanco (mapa_coli, bancos, puntos, 11, 60, 8, 80, FALSE);
	insertarBanco (mapa_coli, bancos, puntos, 25, 10, 22, 20, FALSE);
}

void cargaCuadros ( FILE *f, TMapa mapa_coli ) {
	int i, n_aux, x1, x2, y1, y2, z1, z2;
	int mx1, my1, mx2, my2;
	char nombre_tex[MAX_LONG_NOMBRE];
	if (!feof(f)) { // Lee el numero de cuadros que hay
		fscanf (f, "%d", &n_aux);
		for (i=0; i < n_aux; i++) {	// Carga todos los cuadros
			fscanf (f, "%d %d %d %d %d %d", &x1, &y1, &z1, &x2, &y2, &z2);
			fscanf (f, "%d %d %d %d", &mx1, &my1, &mx2, &my2);	//mascara del area de informacion
			fscanf (f, "%s", &nombre_tex[0]);
			insertarTexturaCuadro (&nombre_tex[0]);
			insertarCuadro (mapa_coli, cuadros, puntos, x1, y1, z1, x2, y2, z2);
			insertarPosicionEspecial (mapa_coli, x1+mx1*AREA_INFO, y1+my1*AREA_INFO, x2+mx2*AREA_INFO, y2+my2*AREA_INFO, CUADRO_1+i);
		}
	}
}

void iniCuadros ( TMapa mapa_coli ) {
	insertarTexturaCuadro (DIB_CUAD_1);
	insertarCuadro (mapa_coli, cuadros, puntos, 40, 51, 2, 25, 51, 9);
	insertarPosicionEspecial (mapa_coli, 40, 50, 25, 50+AREA_INFO, CUADRO_1);
	insertarTexturaCuadro (DIB_CUAD_2);
	insertarCuadro (mapa_coli, cuadros, puntos, 8, 139, 2, 22, 139, 10);
	insertarPosicionEspecial (mapa_coli, 8, 139, 22, 139-AREA_INFO, CUADRO_2);
	insertarTexturaCuadro (DIB_CUAD_3);
	insertarCuadro (mapa_coli, cuadros, puntos, 1, 60, 2, 1, 80, 9);
	insertarPosicionEspecial (mapa_coli, 1, 60, 1+AREA_INFO, 80, CUADRO_3);
	insertarTexturaCuadro (DIB_CUAD_1);
	insertarCuadro (mapa_coli, cuadros, puntos, 79, 83, 2, 79, 57, 10);
	insertarPosicionEspecial (mapa_coli, 79, 83, 79-AREA_INFO, 57, CUADRO_4);
	insertarTexturaCuadro (DIB_CUAD_2);
	insertarCuadro (mapa_coli, cuadros, puntos, 30, 89, 1, 34, 89, 10);
	insertarPosicionEspecial (mapa_coli, 30, 89, 34, 89-AREA_INFO, CUADRO_5);
	insertarTexturaCuadro (DIB_CUAD_3);
	insertarCuadro (mapa_coli, cuadros, puntos, 52, 89, 1, 63, 89, 9);
	insertarPosicionEspecial (mapa_coli, 52, 89, 63, 89-AREA_INFO, CUADRO_6);
	insertarTexturaCuadro (DIB_CUAD_1);
	insertarCuadro (mapa_coli, cuadros, puntos, 63, 51, 1, 52, 51, 9);
	insertarPosicionEspecial (mapa_coli, 63, 51, 52, 51+AREA_INFO, CUADRO_7);
	insertarTexturaCuadro (DIB_CUAD_2);
	insertarCuadro (mapa_coli, cuadros, puntos, 1, 13, 1, 1, 17, 10);
	insertarPosicionEspecial (mapa_coli, 1, 11, 1+AREA_INFO, 19, CUADRO_8);
	insertarTexturaCuadro (DIB_CUAD_3);
	insertarCuadro (mapa_coli, cuadros, puntos, 29, 22, 3, 29, 8, 6);
	insertarPosicionEspecial (mapa_coli, 29, 22, 29-AREA_INFO, 8, CUADRO_9);
}

void cargaPilares ( FILE *f, TMapa mapa_coli ) {
	int i, n_aux, x1, x2, y1, y2, z;
	char nombre_tex[MAX_LONG_NOMBRE];
	if (!feof(f)) { // Lee el numero de pilares
		//fscanf (f, "%s", &nombre_tex[0]);
		//insertarTexturaPilar (&nombre_tex[0]);
		fscanf (f, "%d", &n_aux);
		for (i=0; i < n_aux; i++) {	// Carga todos los pilares
			fscanf (f, "%d %d %d %d %d", &x1, &y1, &x2, &y2, &z);
			insertarPilar (mapa_coli, pilares, puntos, x1, y1, x2, y2, z);
		}
	}
}

void iniPilares ( TMapa mapa_coli ) {
	insertarTexturaPilar (DIB_PILAR);
	insertarPilar (mapa_coli, pilares, puntos, 9, 117, 10, 118, 4);
	insertarPilar (mapa_coli, pilares, puntos, 20, 117, 21, 118, 4);
	insertarPilar (mapa_coli, pilares, puntos, 14, 14, 15, 15, 1);
}

void cargaPosicionesEspeciales ( FILE *f, TMapa mapa_coli ) {
	int i, n_aux, x1, x2, y1, y2, z;
	if (!feof(f)) { // Lee el numero de posiciones especiales
		fscanf (f, "%d", &n_aux);
		for (i=0; i < n_aux; i++) {	// Carga todas las posiciones especiales
			fscanf (f, "%d %d %d %d %d", &x1, &y1, &x2, &y2, &z);
			insertarPosicionEspecial (mapa_coli, x1, y1, x2, y2, ENTRADA+z);
		}
	}
}

void iniPosicionesEspeciales ( TMapa mapa_coli ) {
	insertarPosicionEspecial (mapa_coli, 30, 121, 39, 129, ENTRADA);
}

void cargaCamaras ( FILE *f, TMapa mapa_coli ) {
	int i, n_aux;
	float pos[3], angulo[2];
	extern TCamara camaras[];
	extern short n_camaras;
	if (!feof(f)) { // Lee el numero de camaras que hay
		fscanf (f, "%d", &n_aux);
		for (i=0; i < n_aux; i++) {	// Lee las posiciones de las camaras
			fscanf (f, "%f %f %f %f %f", &pos[0], &pos[1], &pos[2], &angulo[0], &angulo[1]);
			iniPosicionv (&camaras[CAMARA_1+i], pos);
			iniVistav (&camaras[CAMARA_1+i], angulo);
		}
		n_camaras = n_aux + 1; // Hay n_camaras mas una (el jugador)
	}
	if (!feof(f)) { // Lee la posicion del jugador
		fscanf (f, "%f %f %f %f", &pos[0], &pos[1], &angulo[0], &angulo[1]);
		pos[2] = ALTURA;
		iniPosicionv (&camaras[JUGADOR], pos);
		iniVistav (&camaras[JUGADOR], angulo);
	}
}

void cargaLuces ( FILE *f, TMapa mapa_coli ) {
	float pos[3], tam;
	extern GLfloat light0_pos[];
	extern GLfloat light1_pos[];
	extern GLfloat light2_pos[];

	if (!feof(f)) { // Lee la posicion de las luces
		fscanf (f, "%f %f %f %f", &pos[0], &pos[1], &pos[2], &tam);
		light0_pos[0] = pos[0]; light0_pos[1] = pos[1]; light0_pos[2] = pos[2]; light0_pos[3] = tam;
	}
	if (!feof(f)) {
		fscanf (f, "%f %f %f %f", &pos[0], &pos[1], &pos[2], &tam);
		light1_pos[0] = pos[0]; light1_pos[1] = pos[1]; light1_pos[2] = pos[2]; light1_pos[3] = tam;
	}
	if (!feof(f)) {
		fscanf (f, "%f %f %f %f", &pos[0], &pos[1], &pos[2], &tam);
		light2_pos[0] = pos[0]; light2_pos[1] = pos[1]; light2_pos[2] = pos[2]; light2_pos[3] = tam;
	}
}

void cargaFichero ( FILE *f, TMapa mapa_coli ) {
	cargaParedes (f, mapa_coli);
	cargaBancos(f, mapa_coli);
	cargaCuadros (f, mapa_coli);
	cargaPilares (f, mapa_coli);
	cargaPosicionesEspeciales (f, mapa_coli);
	cargaCamaras (f, mapa_coli);
	cargaLuces (f, mapa_coli);
}

void cargaNormal ( TMapa mapa_coli ) {
	iniParedes (mapa_coli);
	iniBancos (mapa_coli);
	iniCuadros (mapa_coli);
	iniPilares (mapa_coli);
	iniPosicionesEspeciales (mapa_coli);
}

void cargarMundo ( TMapa mapa_coli ) {
	FILE *file;
	
	iniColisiones ( mapa_coli );
		printf ("Creando mundo...\n");

	if ((file = fopen (FICH_DATOS, "rb")) == NULL) {
		printf("No se encuentra el fichero de datos: %s\nSe procede a cargar opciones por defecto\n", FICH_DATOS);
		cargaNormal (mapa_coli);
	} else {
		printf("Cargando mundo desde fichero: %s\n", FICH_DATOS);
		cargaFichero (file, mapa_coli);
		fclose (file);
	}
			printf ("Cargando el mundo creado...\n");
	//Crea listas de Visual
	creaSueloTecho ( );
	creaListaParedes ( paredes );
	creaListaBancos ( bancos );
	creaListaCuadros ( cuadros );
	creaListaPilares ( pilares );
		printf ("Mundo cargado con exito\n");
}
