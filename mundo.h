#ifndef _MUNDO_
#define _MUNDO_

#define DL_SUELO    1
#define DL_PAREDES  2
#define DL_BANCOS   3
#define DL_CUADROS  4
#define DL_PILARES  5
#define DL_HUD      6
#define DL_MAPA     7
#define DL_LUCES    8

typedef float TPared [5][3];

typedef float TPuntos [N_PUNTOS][2];
typedef float TParedes [N_PAREDES][4];
typedef	float TBancos [N_BANCOS][4];
typedef float TCuadros [N_CUADROS][6];
typedef float TPilares [N_PILARES][5];

void insertarPunto (TPuntos puntos, int x, int y);
void insertarCuadrado (TPuntos puntos, int x1, int y1, int x2, int y2);

void insertarTexturaPared (char *nombre_tex);
void insertarTexturaSuelo (char *nombre_tex);
void insertarTexturaTecho (char *nombre_tex);
void insertarTexturaBanco (char *nombre_tex);
void insertarTexturaCuadro (char *nombre_tex);
void insertarTexturaPilar (char *nombre_tex);

void insertarPared (TMapa mapa_coli, TParedes paredes, int x1, int y1, int x2, int y2);
void insertarBanco (TMapa mapa_coli, TBancos bancos, TPuntos puntos, int x1, int y1, int x2, int y2, boolean es_vertical);
void insertarCuadro (TMapa mapa_coli, TCuadros cuadros, TPuntos puntos, int x1, int y1, int z1, int x2, int y2, int z2);
void insertarPilar (TMapa mapa_coli, TPilares pilares, TPuntos puntos, int x1, int y1, int x2, int y2, int z);

void creaListaParedes ( TParedes paredes );
void creaSueloTecho ( void );
void creaListaBancos ( TBancos bancos );
void creaListaCuadros ( TCuadros cuadros );
void creaListaPilares ( TPilares pilares );

void pintarEscenario ( void );

#endif
