#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include "practica5.h"
#include "imagenes.h"

TTexturas texturas;
short n_texturas = 0;

static unsigned int getint (FILE *fp) {
  int c, c1, c2, c3;

  /* obtiene 4 bytes*/
  c = getc(fp);  
  c1 = getc(fp);  
  c2 = getc(fp);  
  c3 = getc(fp);
  
  return ((unsigned int) c) +   
    (((unsigned int) c1) << 8) + 
    (((unsigned int) c2) << 16) +
    (((unsigned int) c3) << 24);
}

static unsigned int getshort (FILE *fp) {
	int c, c1;

  /*obtiene 2 bytes*/
  c = getc(fp);  
  c1 = getc(fp);
 
  return ((unsigned int) c) + (((unsigned int) c1) << 8);
}

/* 'write_word()' - Write a 16-bit unsigned integer. */
static int write_word (FILE *fp, unsigned short w) {
	putc(w, fp);
	return (putc(w >> 8, fp));
}

/*'write_dword()' - Write a 32-bit unsigned integer. */
static int write_dword (FILE *fp, unsigned int dw) {
	putc(dw, fp);
	putc(dw >> 8, fp);
	putc(dw >> 16, fp);
	return (putc(dw >> 24, fp));
}

/*'write_long()' - Write a 32-bit signed integer. */
static int write_long (FILE *fp, int l) {
	putc(l, fp);
	putc(l >> 8, fp);
	putc(l >> 16, fp);
	return (putc(l >> 24, fp));
}

/*'imageSave()' - Save a DIB/BMP file to disk.
 * Returns 0 on success or -1 on failure... */
int imageSave (const char *filename, BITMAPINFO *info, GLubyte *bits) {
	FILE *fp;                      /* Open file pointer */
	int  size,                     /* Size of file */
			 infosize,                 /* Size of bitmap info */
			 bitsize;                  /* Size of bitmap pixels */
	//BITMAPINFO *info;

	/* Try opening the file; use "wb" mode to write this *binary* file. */
	if ((fp = fopen(filename, "wb")) == NULL)
			return (-1);

	/* Figure out the bitmap size */
	if (info->bmiHeader.biSizeImage == 0)
		bitsize = (info->bmiHeader.biWidth * info->bmiHeader.biBitCount + 7) / 8 * abs(info->bmiHeader.biHeight);
	else
		bitsize = info->bmiHeader.biSizeImage;

	/* Figure out the header size */
	infosize = sizeof(BITMAPINFOHEADER);
	switch (info->bmiHeader.biCompression) {
		case BI_BITFIELDS :
				infosize += 12; /* Add 3 RGB doubleword masks */
				if (info->bmiHeader.biClrUsed == 0)
					break;
		case BI_RGB :
				if (info->bmiHeader.biBitCount > 8 &&	info->bmiHeader.biClrUsed == 0)
					break;
		case BI_RLE8 :
		case BI_RLE4 :
				if (info->bmiHeader.biClrUsed == 0)
						infosize += (1 << info->bmiHeader.biBitCount) * 4;
				else
						infosize += info->bmiHeader.biClrUsed * 4;
				break;
	}

	size = sizeof(BITMAPFILEHEADER) + infosize + bitsize;

	/* Write the file header, bitmap information, and bitmap pixel data... */
	write_word(fp, BF_TYPE);        /* bfType */
	write_dword(fp, size);          /* bfSize */
	write_word(fp, 0);              /* bfReserved1 */
	write_word(fp, 0);              /* bfReserved2 */
	write_dword(fp, 18 + infosize); /* bfOffBits */

	write_dword(fp, info->bmiHeader.biSize);
	write_long(fp, info->bmiHeader.biWidth);
	write_long(fp, info->bmiHeader.biHeight);
	write_word(fp, info->bmiHeader.biPlanes);
	write_word(fp, info->bmiHeader.biBitCount);
	write_dword(fp, info->bmiHeader.biCompression);
	write_dword(fp, info->bmiHeader.biSizeImage);
	write_long(fp, info->bmiHeader.biXPelsPerMeter);
	write_long(fp, info->bmiHeader.biYPelsPerMeter);
	write_dword(fp, info->bmiHeader.biClrUsed);
	write_dword(fp, info->bmiHeader.biClrImportant);

	if (infosize > 40)
		if (fwrite(info->bmiColors, infosize - 40, 1, fp) < 1) {
			/* Couldn't write the bitmap header - return... */
			fclose(fp);
			return (-1);
		}

	if (fwrite(bits, 1, bitsize, fp) < bitsize) {
		/* Couldn't write the bitmap - return... */
		fclose(fp);
		return (-1);
	}

	/* OK, everything went fine - return... */
	fclose(fp);
	return (0);
}

void imprimirPantalla (int ancho, int alto) {
	BITMAPINFOHEADER bmiHeader;
	unsigned char *data;
	char temp;
	unsigned int bmp_tam;
	unsigned short int bpp = 3;	//Numero de bytes por pixel
	unsigned long x, y;
	int swapY, offset, swapOffset;

	bmp_tam = ancho * alto * bpp;	// El tama�o de la imagen BMP
	//Reservamos memoria para guardar la imagen
	data = (char *)malloc (bmp_tam);
	
	//Leemos la imagen de la pantalla
	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glReadPixels (0, 0, ancho, alto, GL_RGB, GL_UNSIGNED_BYTE, data);

	//glReadPixels nos deja la imagen al reves, asique tenemos que invertirla
	for (y = 0; y < alto / 2; y++) {
		swapY = alto - y - 1;
		for(x = 0; x < ancho; x++) {
			offset = bpp * (x + y * ancho);
			swapOffset = bpp * (x + swapY * ancho);
			// Cambia los RGB de los dos pixels y los pasa de (rgb -> bgr) 
			temp = data[offset + 0]; data[offset + 0] = data[swapOffset + 2]; data[swapOffset + 2] = temp;
			temp = data[offset + 1]; data[offset + 1] = data[swapOffset + 1]; data[swapOffset + 1] = temp;
			temp = data[offset + 2]; data[offset + 2] = data[swapOffset + 0]; data[swapOffset + 0] = temp;
		}
	}
	
	//TODO: Rellenar la cabecera info del BMP -> BITMAPINFOHEADER

}

/* cargador rapido de un mapa de bits...de 24 bits con solo un plano.  */
/* See http://www.dcs.ed.ac.uk/~mxr/gfx/2d/BMP.txt for more info.*/
int imageLoad (char *filename, TImagen *img) {
	FILE *file;
	unsigned long i;
	unsigned short int planes;          /* numero de planos en la imagen (debe ser 1) */
	unsigned short int bpp;             /* numero de bits por pixel (debe ser 24) */
	char temp;                          /* temp para pasar de bgr-rgb */

	/* Abre el fichero. */
	if ((file = fopen(filename, "rb"))==NULL) {
		printf("Fichero no encontrado : %s\n",filename);
		return 0;
	}
	
	/* pasa la cabecera del bmp hasta encontrar el width/height: */
	fseek(file, 18, SEEK_CUR);

	/* lectura del ancho */
	img->width = getint (file);
		//printf("Anchura de %s: %lu\n", filename, img->width);
	
	/* Lectura del alto */
	img->height = getint (file);
		//printf("Altura de %s: %lu\n", filename, img->height);
	
	/* calcula el tama�o (asumiendo 24 bits o 3 bytes por pixel).*/
	img->tam = img->width * img->height * 3;

	/* Lectura de los planos */
	planes = getshort(file);
	if (planes != 1) {
		printf("Planos de %s distinto de 1: %u\n", filename, planes);
		return 0;
	}

	/* Lectura de los bpp */
	bpp = getshort(file);
	if (bpp != 24) {
		printf("Bpp de %s es distinto de 24: %u\n", filename, bpp);
		return 0;
	}

	/* pasa el resto de la cabecera bmp. */
	fseek(file, 24, SEEK_CUR);

	/* Lectura de la imagen. */
	img->data = (char *) malloc(img->tam);
	if (img->data == NULL) {
		printf("Error obteniendo memoria para almacenar los datos de la imagen");
		return 0;
	}

	if ((i = fread(img->data, img->tam, 1, file)) != 1) {
		printf("Error leyendo los datos de imagen de %s.\n", filename);
		return 0;
	}

	for (i=0;i<img->tam;i+=3) { /* se dan la vuelta a todos los colores. (bgr -> rgb) */
		temp = img->data[i];
		img->data[i] = img->data[i+2];
		img->data[i+2] = temp;
	}

	return 1;
}

/* Carga la textura y devuelve el identificador de la textura */
int carga_textura (char *filename) {
	TImagen tex;
	GLuint tex_ID;

	imageLoad (filename, &tex);
	if (tex.data == NULL) {
		return -1;
	}

	/* Genera una textura, referenciada por el entero texID */
	glGenTextures (1, &tex_ID);

	/* Esta funci�n indica que ser� una textura en 2D. Las siguientes funciones har� referncia a esta textura */
	glBindTexture (GL_TEXTURE_2D, tex_ID);

	/* Aqu� ajustamos algunos par�metros de la textura, concretamente los filtros */
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	/* Con esta funci�n de GLU, creamos un mipmap. N�tese que especificamos el tama�o con tam, que devolvia la funci�n CargaTGA*/
	//gluBuild2DMipmaps (GL_TEXTURE_2D, 3, tex.tam, tex.tam, GL_RGB,GL_BITMAP, tex.data);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, tex.width, tex.height, 0, GL_RGB, GL_UNSIGNED_BYTE, tex.data);

	/* Liberamos la memoria que ya no utilizaremos */
	free(tex.data);

	return tex_ID;
}
