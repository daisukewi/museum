#
#Makefile para la practica 5
#
CC = gcc
LIBS = -I /usr/include/GL/ -L /usr/include/GL/ -lGL -lGLU -lglut -lX11
CFLAGS =
DEBUG = -DDEBUG
OBJETOS = mundo.o imagenes.o camaras.o
LIBRERIAS =  practica5.h mundo.h imagenes.h camaras.h

Practica5: Practica5.o ${OBJETOS}
	${CC} ${OBJETOS} Practica5.o -o practica ${CFLAGS} ${LIBS}

Practica5.o: Practica5.c ${LIBRERIAS}
	${CC} -c Practica5.c -o Practica5.o ${CFLAGS}

mundo.o: mundo.c ${LIBRERIAS}
	${CC} -c mundo.c -o mundo.o ${CFLAGS}

imagenes.o: imagenes.c ${LIBRERIAS}
	${CC} -c imagenes.c -o imagenes.o ${CFLAGS}
	
camaras.o: camaras.c ${LIBRERIAS}
	${CC} -c camaras.c -o camaras.o ${CFLAGS}

clean:
	rm -f Practica5 *.o

run: Practica5
	./practica
