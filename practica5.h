#ifndef _PRINCIPAL_
#define _PRINCIPAL_
/*
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include <GL/glaux.h>
*/
#define MI_GLUT	1

#define PI        3.14159265
#define MAX_LONG_NOMBRE	20
#define FICH_DATOS	"res\\mapa.txt"
#define RES_DIR		"res"

//Constantes de nombres de texturas
#define DIB_PAREDES	"res\\muro.bmp"
#define DIB_SUELO		"res\\suelo.bmp"
#define DIB_TECHO		"res\\techo.bmp"
#define DIB_BANCO		"res\\banco.bmp"
#define DIB_PILAR		"res\\banco.bmp"
#define DIB_CUAD_1	"res\\cuadro1.bmp"
#define DIB_CUAD_2	"res\\cuadro2.bmp"
#define DIB_CUAD_3	"res\\cuadro3.bmp"

//Constantes de ventana
#define WIDTH     1024
#define HEIGHT    768
#define MARCO_H		60
#define MARCO_V		40
#define ALTURA_MARCO	60
#define CARAS_CIRCULO	100

//Constantes de movimiento
#ifdef MI_GLUT
	#define VELOCIDAD 0.2
	#define VELOCIDAD_LATERAL	VELOCIDAD/2
#else
	#define VELOCIDAD 0.5
	#define VELOCIDAD_LATERAL VELOCIDAD
#endif
#define PRECISION	546.0
#define DIST      0.1
#define GIRO      PI/128
#define GIRO_MAX  PI/3      // 60� de giro vertical maximo de cabeza
#define GIRO_MIN  -GIRO_MAX // -60� de giro vertical minimo de cabeza

//Constantes de objetos y paredes
#define LIM_X     81    // Limites de las paredes en el eje x 81
#define LIM_Y     141	   // en el eje y 141
#define LIM_Z     10.0   // en el eje z
#define N_TEXTURAS	20
#define N_PAREDES 24      // Numero de paredes que se van a crear
#define N_BANCOS	7
#define N_CUADROS	9
#define N_PILARES 3
#define N_PUNTOS	N_PAREDES+(N_BANCOS*4)+(N_CUADROS*4)+(N_PILARES*4)
#define TAM_PARED 0      // Grosor en pixels de las paredes, a parte del que define la propia pared:
                         // 0 -> 1 pixel
                         // 1 -> 3 pixels
                         // 2 -> 5 pixels
#define BANCO_ALTURA_MIN	0.8
#define BANCO_ALTURA_MAX	1.5
#define BANCO_ANCHO_PATA	1.0
#define MOV_COLI	0.1	// Movimiento del jugador despues de colisionar
#define DIST_COLI	0.5		// Distancia de la pared al rango de colision
#define AREA_INFO	12

//Constantes de posicion
#define ALFA_INI	0.0
#define BETA_INI	0.0
#define X_INI			35.0
#define Y_INI			125.0
#define ALTURA    3.6
#define MEDIA_ALTURA	1.5

//Constantes de camaras
#define TICK_INTERVAL	30
#define GIRO_CAM		PI/280

//Posicion camara 1
#define X_CAM_1			2.0
#define Y_CAM_1			138.0
#define Z_CAM_1			9.0
#define ALFA_CAM_1	-PI/4
#define BETA_CAM_1	-PI/12

//Posicion camara 2
#define X_CAM_2			2.0
#define Y_CAM_2			88.0
#define Z_CAM_2			9.0
#define ALFA_CAM_2	-PI/4
#define BETA_CAM_2	-PI/12

//Posicion camara 3
#define X_CAM_3			78.0
#define Y_CAM_3			52.0
#define Z_CAM_3			9.0
#define ALFA_CAM_3	6*PI/7
#define BETA_CAM_3	-PI/12

//Posicion camara 4
#define X_CAM_4			15.0
#define Y_CAM_4			2.0
#define Z_CAM_4			9.0
#define ALFA_CAM_4	PI/2
#define BETA_CAM_4	-PI/12

//Posicion camara giratoria
#define X_CAM_G			15.0
#define Y_CAM_G			70.0
#define Z_CAM_G			9.0
#define ALFA_CAM_G	0.0
#define BETA_CAM_G	-PI/12

#endif
