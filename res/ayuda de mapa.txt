5 //numero de paredes
0 0 0 80 //coordenadas de las paredes x1, y1, x2, y2
0 80 10 80
20 80 80 80
80 80 80 0
80 0 0 0
2 //numero de habitaciones
0 0 80 80 //coordenadas de las habitaciones x1, y1, x2, y2
10 80 20 100
1 //numero de bancos
20 20 30 22 1 //coordenadas de los bancos x1, y1, x2, y2, si es vertical 1 si no 0
1 //numero de cuadros
20 40 1 20 46 9 //coordenadas x1, y1, z1, x2, y2, z2
0 0 1 0 //mascara de area de info si hay un 0 no se avanza, 1 se avanza
1 //numero de pilares
70 70 71 71 3 //coordenadas x1, y1, x2, y2
1 //numero de posiciones especiales
1 1 10 10 0 //coordenadas x1, y1, x2, y2, numero de posicion
1 //numero de camaras
1.0 1.0 8.0 0.0 0.0 //coordenadas x, y, z, alfa y beta
5.0 5.0 0.0 0.0 //coordenadas de la posicion inicial x, y, alfa y beta
15.0 15.0 8.0 1.0 //coordenadas de las luces x, y, z, alfa. Luz0
40.0 40.0 8.0 1.0 //Luz1
80.0 140.0 9.0 1.0 //Luz2