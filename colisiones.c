#include "practica5.h"
#include "colisiones.h"
#include "camaras.h"

/* Inicializa el mapa de colisiones */
void iniColisiones ( TMapa mapa_coli ) {
	int i, j;
	for (i=0; i<LIM_X; i++)
		for (j=0; j<LIM_Y; j++)
			mapa_coli[i][j] = NO_COLI;
}

/* Inserta una casilla de un tipo en el mapa de colisiones */
void insertarPosicionEspecial (TMapa mapa_coli, int x1, int y1, int x2, int y2, TColision pos) {
	int i, j;
	i = MIN(x1,x2);
	x2 = MAX(x1,x2);
	x1 = i;
	j = MIN(y1,y2);
	y2 = MAX(y1,y2);
	y1 = j;
	for (i = x1-TAM_PARED; i <= x2+TAM_PARED; i++)
		for (j = y1-TAM_PARED; j <= y2+TAM_PARED; j++)
			if (mapa_coli[i][j] != COLI) mapa_coli[i][j] = pos;
}

/* Inserta una casilla de tipo colision */
void insertarColision (TMapa mapa_coli, int x1, int y1, int x2, int y2) {
	printf("(%d,%d)(%d,%d)\n",x1,y1,x2,y2);
	insertarPosicionEspecial (mapa_coli, x1, y1, x2, y2, COLI);
}

/*
void insertarColiPared (TMapa mapa_coli, int x1, int y1, int x2, int y2) {
	int x, y;
	float m;
	x = MIN(x1,x2);
	x2 = MAX(x1,x2);
	x1 = x;
	y = MIN(y1,y2);
	y2 = MAX(y1,y2);
	y1 = y;
	
	printf("(%d,%d)(%d,%d)\n",x1,y1,x2,y2);	
	if (x1 = x2)
		for (y = y1; y <= y2; y++)
			mapa_coli[x1][y] = COLI;
	else if (y1 = y2)
		for (x = x1; x <= x2; x++)
			mapa_coli[x][y1] = COLI;
	else {
		m = (y2 - y1) / (x2 - x1);
		for (x = x1; x <= x2; x++){
			y = (int)(m * (x - x1) + y1);
			mapa_coli[x][y] = COLI;
		}
	}
}*/

/* Imprime el mapa actual en la salida estandar. Utilizado para gestion de errores */
void imprimirMapa (TMapa mapa_coli, TCamara *obj) {
	int i, j;
	for (i=0; i<LIM_X; i++){
		printf("\n");
		for (j=0; j<LIM_Y; j++){
			if ((i == (int)(obj->eyex)) && (j == (int)(obj->eyey)))
				printf("  x ");
			else
				printf("%3i ",mapa_coli[i][j]);
		}
	}
	printf("\n");
}

/* Comprueba que no haya colisiones y devuelve el tipo de casilla en la que esta el jugador. */
TColision comprobarColision (TMapa mapa_coli, TCamara *jugador) {
	float posx, posy;
	unsigned char colisionado;
	posx = jugador->eyex;
	posy = jugador->eyey;
	
	colisionado = (posx>=0.0) && (posx<=LIM_X) && (posy>=0.0) && (posy<=LIM_Y);
	while (colisionado){
		colisionado = 0;
		if (mapa_coli[(int)posx][(int)(posy-1)] == COLI){ //Colision superior
			posy += MOV_COLI; colisionado = 1;
		}
		if (mapa_coli[(int)posx][(int)(posy+1)] == COLI){ //Colision inferior
			posy -= MOV_COLI; colisionado = 1;
		}
		if (mapa_coli[(int)(posx-1)][(int)(posy)] == COLI){ //Colision izquierda
			posx += MOV_COLI; colisionado = 1;
		}
		if (mapa_coli[(int)(posx+1)][(int)posy] == COLI){ //Colision derecha
			posx -= MOV_COLI; colisionado = 1;
		}
		/*if (!colisionado){
			if (mapa_coli[(int)(posx-1)][(int)(posy-1)] == COLI){ //Colision superior-izquierda
				posx += MOV_COLI; posy += MOV_COLI; colisionado = 1;
			} else if (mapa_coli[(int)(posx-1)][(int)(posy+1)] == COLI){ //Colision superior-derecha
				posx += MOV_COLI; posy -= MOV_COLI; colisionado = 1;
			} else if (mapa_coli[(int)(posx+1)][(int)(posy+1)] == COLI){ //Colision inferior-derecha
				posx -= MOV_COLI; posy -= MOV_COLI; colisionado = 1;
			} else if (mapa_coli[(int)(posx+1)][(int)(posy-1)] == COLI){ //Colision inferior-izquierda
				posx -= MOV_COLI; posy += MOV_COLI; colisionado = 1;
			}
		}*/
	}
	jugador->eyex = posx;
	jugador->eyey = posy;
	return (mapa_coli[(int)posx][(int)posy]);
}
