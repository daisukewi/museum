#include <GL/glut.h>
//#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "practica5.h"
#include "colisiones.h"
#include "camaras.h"
#include "mundo.h"

GLfloat color_hud [] = { 0.0f, 0.0f, 0.0f };
GLfloat color_pared_mapa [] = {0.0f,1.0f,1.0f};
GLfloat color_objetos_mapa [] = {0.0f,0.0f,1.0f};


void iniVistaVertical ( TCamara *obj, float angulo ) {
	iniVista (obj, obj->alfa, angulo);
}

void iniVistaHorizontal ( TCamara *obj, float angulo ) {
	iniVista (obj, angulo, obj->beta);
}

/* Pone la proyeccion Ortografica */
void setOrthographicProjection (int ancho, int alto)
{
	glDisable (GL_LIGHTING);
	glDisable (GL_TEXTURE_2D);
	glDisable (GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);     // switch to projection mode
	glPushMatrix();                  // save previous matrix which contains the settings for the perspective projection
	  glLoadIdentity();              // reset matrix
	  gluOrtho2D(0, ancho, 0, alto); // set a 2D orthographic projection
	  glScalef(1, -1, 1);            // invert the y axis, down is positive
	  glTranslatef(0, -alto, 0);   // move the origin from the bottom left corner
	  glMatrixMode(GL_MODELVIEW);    // to the upper left corner
}

/* Restaura la proyeccion Perspectiva */
void resetPerspectiveProjection (void) {
	  glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glEnable (GL_DEPTH_TEST);
	glEnable (GL_TEXTURE_2D);
	glEnable (GL_LIGHTING);
}

/* Escribe una cadena de texto en la posicion X, Y */
void renderBitmapString(float x, float y, void *font, char *string) {
	char *c;
	glRasterPos2f(x, y);
	for (c=string; *c != '\0'; c++) {
		glColor3f(1.0,0.0,0.0);
		glutBitmapCharacter(font, *c);
	}
}

/* Crea la lista de visualizacion del HUD */
void cargaHUD (int ancho, int alto) {
	glNewList (DL_HUD, GL_COMPILE);
		glPushMatrix();
			glLoadIdentity();
			glBegin(GL_QUADS);
				glColor3fv (color_hud);
				glVertex2f (MARCO_H, alto-MARCO_V-ALTURA_MARCO);
				glVertex2f (ancho-MARCO_H, alto-MARCO_V-ALTURA_MARCO);
				glVertex2f (ancho-MARCO_H, alto-MARCO_V);
				glVertex2f (MARCO_H, alto-MARCO_V);
			glEnd();
		glPopMatrix();
	glEndList();
}

/* Crea la lista de visualizacion del mapa */
void cargaMapa (int ancho, int alto) {
	int i, px, py;
	extern short n_habitaciones;
	extern short n_puntos;
	extern TPuntos puntos;
	ancho -= 2 * MARCO_H;
	alto -= 2 * MARCO_V;
	glNewList (DL_MAPA, GL_COMPILE);
		glPushMatrix();
			glLoadIdentity();
			glBegin(GL_QUADS);
				for (i=0; i<n_puntos; i++){
					px = ((ancho * puntos[i][1]) / LIM_Y) + MARCO_H;
					py = ((alto * puntos[i][0]) / LIM_X) + MARCO_V;
					if (i<n_habitaciones*4) glColor3fv (color_pared_mapa); else glColor3fv (color_objetos_mapa);
					glVertex2f (px,py);
				}
			glEnd();
		glPopMatrix();
	glEndList();
}

/* Carga el HUD en pantalla */
void imprimeHUD (int ancho, int alto, TColision pos) {
	char label[81];
	setOrthographicProjection(ancho, alto);
	glCallList (DL_HUD);
	glPushMatrix();
		glLoadIdentity();
		glPointSize (0.0);
		glBegin(GL_POINTS);
			glColor3f(1.0f,1.0f,1.0f);
			glVertex2f (-1.0,-1.0);
		glEnd();
		switch (pos){
			case CAMARA_1:	sprintf(label, "Camara 1: Camara de vigilancia de la entrada principal.");
							break;
			case CAMARA_2:	sprintf(label, "Camara 2: Camara de vigilancia de la sala grande.");
							break;
			case CAMARA_3:	sprintf(label, "Camara 3: Camara de vigilancia de la sala grande.");
							break;
			case CAMARA_4:	sprintf(label, "Camara 4: Camara de vigilancia de la sala pequenia.");
							break;
			case CAMARA_G:	sprintf(label, "Camara Giratoria: Camara de vigilancia giratoria de todo el museo.");
							break;
			case ENTRADA: sprintf(label, "Entrada: Esta es la entrada principal.");
							break;
			case CUADRO_1: sprintf(label, "Cuadro 1: Las mininas, son pequenitas y juguetonas (^_^)");
							break;
			case CUADRO_2: sprintf(label, "Cuadro 2: Este cuadro si que mola. Ahi to rosa.");
							break;
			case CUADRO_3: sprintf(label, "Cuadro 3: Cuadro del banco grande. Tambien llamado banco que se sujeta con solo 2 patas laaargas.");
							break;
			case CUADRO_4: sprintf(label, "Cuadro 4: El cuadro que esta en la otra punta del mundo. Para ver este cuadro hay que andar demasiado.");
							break;
			case CUADRO_5: sprintf(label, "Cuadro 5: Cuadro fino, pero no tanto.");
							break;
			case CUADRO_6: sprintf(label, "Cuadro 6: Un cuadro normal y corriente... sin sentido.");
							break;
			case CUADRO_7: sprintf(label, "Cuadro 7: Me he quedado sin inspiracion para este cuadro. Lo llamare Aurelio.");
							break;
			case CUADRO_8: sprintf(label, "Cuadro 8: Un ejemplo de cuadro fino. Este cuadro fue pintado por pocosso en el anio fino.");
							break;
			case CUADRO_9: sprintf(label, "Cuadro 9: El cuadro apaisado. Se puede ver el horizonte, pero no el vertical.");
							break;
			default: sprintf(label, "No se tiene informacion sobre este cuadro. Disculpen las molestias");
							break;
		}
		renderBitmapString (MARCO_H+5.0, alto-MARCO_V-(ALTURA_MARCO/2)+5, GLUT_BITMAP_HELVETICA_18, label);
	glPopMatrix();
	resetPerspectiveProjection();
}

/* Pinta el mapa en pantalla */
void pintarMapa (TCamara *obj, int ancho, int alto) {
	float posx, posy;

	setOrthographicProjection(ancho, alto);
		ancho -= 2 * MARCO_H;
		alto -= 2 * MARCO_V;
		posx = ((ancho * obj->eyey) / LIM_Y) + MARCO_H - DIST_COLI;
		posy = ((alto * obj->eyex) / LIM_X) + MARCO_V - DIST_COLI;
		glCallList (DL_MAPA);
		glPushMatrix();
			glLoadIdentity();
			glPointSize (10.0);
			glBegin(GL_POINTS);
				glColor3f(1.0f,0.0f,0.0f);
				glVertex2f (posx,posy);
			glEnd();
		glPopMatrix();
	resetPerspectiveProjection();
}
