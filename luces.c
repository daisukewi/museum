#include <GL/glut.h>
#include "practica5.h"
#include "mundo.h"
#include "luces.h"

GLfloat red[] = {1.0, 0.0, 0.0, 1.0};
GLfloat green[] = {0.0, 1.0, 0.0, 1.0};
GLfloat blue[] = {0.0, 0.0, 1.0, 1.0};
GLfloat yellow[] = {1.0, 1.0, 0.0, 1.0};
GLfloat magenta[] = {1.0, 0.0, 1.0, 1.0};
GLfloat white[] = {1.0, 1.0, 1.0, 1.0};
GLfloat dim[] = {0.5, 0.5, 0.5, 1.0};

GLfloat difusa_encendida[] = { 0.7f, 0.7f, 0.7f, 1.0f}; // luz blanca
GLfloat difusa_apagada[] = { 0.1f, 0.1f, 0.1f, 1.0f}; // luz negra
GLfloat mat_ambient[] = {0.7f, 0.7f, 0.7f, 1.0f}; //gris
GLfloat mat_diffuse[] = {0.6f, 0.6f, 0.6f, 1.0f};
GLfloat mat_specular[] = {0.1f, 0.1f, 0.1f, 1.0f};
GLfloat mat_shininess[]= {50.0f};

GLfloat light0_pos[] = { 15.0f, 15.0f, 8.0f, 1.0f }; // posición de la luz 0
GLfloat light1_pos[] = { 40.0f, 70.0f, 8.0f, 1.0f }; // posición de la luz 1
GLfloat light2_pos[] = { 15.0f, 125.0f, 8.0f, 1.0f }; // posición de la luz 3
GLfloat light0_mano[] = { 0.0f, 0.0f, 0.0f, 1.0f }; // posición de la luz 0

LightInfo linfo[] = {
  { {-4.0, 0.0, -10.0, 1.0}, yellow},
  { {4.0, 0.0, -10.0, 1.0}, green},
  { {-4.0, 0.0, -6.0, 1.0}, red},
  { {4.0, 0.0, -6.0, 1.0}, blue},
  { {-4.0, 0.0, -2.0, 1.0}, green},
  { {4.0, 0.0, -2.0, 1.0}, yellow},
  { {-4.0, 0.0, 2.0, 1.0}, blue},
  { {4.0, 0.0, 2.0, 1.0}, red},
  { {-4.0, 0.0, 6.0, 1.0}, yellow},
  { {4.0, 0.0, 6.0, 1.0}, green},
  { {-4.0, 0.0, 10.0, 1.0}, red},
  { {4.0, 0.0, 10.0, 1.0}, blue},
};

int lightState[8] = {1, 1, 1, 1, 1, 1, 1, 1};

#define MAX_LIGHTS (sizeof(linfo)/sizeof(linfo[0]))

void iniLight (int num) {
  glLightf(GL_LIGHT0 + num, GL_CONSTANT_ATTENUATION, 0.0);
  if (attenuation == M_LINEAR) {
    glLightf(GL_LIGHT0 + num, GL_LINEAR_ATTENUATION, 0.4);
    glLightf(GL_LIGHT0 + num, GL_QUADRATIC_ATTENUATION, 0.0);
  } else {
    glLightf(GL_LIGHT0 + num, GL_LINEAR_ATTENUATION, 0.0);
    glLightf(GL_LIGHT0 + num, GL_QUADRATIC_ATTENUATION, 0.1);
  }
  glLightfv(GL_LIGHT0 + num, GL_SPECULAR, dim);
}

/* Draw a sphere the same color as the light at the light position so it is
   easy to tell where the positional light sources are. */
void drawLight (LightInfo * info) {
  glPushMatrix();
	  glTranslatef (info->xyz[0], info->xyz[1], info->xyz[2]);
	  glColor3fv (info->rgb);
	  glCallList (DL_LUCES);
  glPopMatrix();
}

void cargaLuces ( void ) {
	glNewList(DL_LUCES, GL_COMPILE);
		glutSolidSphere (0.4, CARAS_CIRCULO, CARAS_CIRCULO);
  glEndList();
}

void pintarLuces ( void ) {
	int i;

  glDisable(GL_LIGHTING);
  for (i = 0; i < MAX_LIGHTS; i++) {
    drawLight (&linfo[i]);
  }
  glEnable(GL_LIGHTING);

  for (i = 0; i < numActiveLights; i++) {
    if (lightState[i]) {
      int num = ld[i].num;

      glLightfv(GL_LIGHT0 + i, GL_POSITION, linfo[num].xyz);
      glLightfv(GL_LIGHT0 + i, GL_DIFFUSE, linfo[num].rgb);
      glEnable(GL_LIGHT0 + i);
    } else {
      glDisable(GL_LIGHT0 + i);
    }
  }
}

void iniLuces ( void ) {
	//Lo que yo hago
	glLightfv(GL_LIGHT0, GL_DIFFUSE, difusa_encendida); // Se asignan los parámetros a la luz 0
	glEnable(GL_LIGHT0); // Se enciende la luz 0
	
	glLightfv(GL_LIGHT1, GL_DIFFUSE, difusa_encendida); // Se asignan los parámetros a la luz 1
	glEnable(GL_LIGHT1); // Se enciende la luz 1
	
	glLightfv(GL_LIGHT2, GL_DIFFUSE, difusa_encendida); // Se asignan los parámetros a la luz 2
	glEnable(GL_LIGHT2); // Se enciende la luz 2
	
	glShadeModel(GL_SMOOTH); // Se define el modelo suave
	
	glColorMaterial(GL_FRONT, GL_DIFFUSE);  //caras del objeto afectadas por el material
	glMaterialfv(GL_FRONT,GL_AMBIENT,mat_ambient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,mat_diffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,mat_specular);
	glMaterialfv(GL_FRONT,GL_SHININESS,mat_shininess);
	//Ya no hago mas
}
