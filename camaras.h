#ifndef _CAMARAS_
#define _CAMARAS_

#include "colisiones.h"

typedef unsigned char boolean;

#define TRUE	1
#define FALSE	0

#define N_CAMARAS	6
#define JUGADOR		0		// Camra del jugador
#define CAMARA_1	1		// Camaras del 1 - 4
#define CAMARA_2	2
#define CAMARA_3	3
#define CAMARA_4	4
#define CAMARA_G	5		// Camara giratoria
#define VISTA_CAMARAS	6		//Se ven las 4 camaras juntas

// estados de las teclas
#define N_ESTADOS	12
#define AVANCE		0
#define RETROCESO	1
#define IZQUIERDA	2
#define DERECHA		3
#define GIRO_IZQ	4
#define GIRO_DER	5
#define GIRO_UP		6
#define GIRO_DOWN	7
#define BOTON_IZQ	8
#define BOTON_DER	9
#define	AGACHADO	10
#define	VISTA_MAPA	11

typedef struct {
        float eyex, eyey, eyez;
        float atx, aty, atz;
        float upx, upy, upz;
        float alfa, beta;
} TCamara;
typedef TCamara TCamaras [N_CAMARAS];

typedef boolean TKeyState [N_ESTADOS];

void girar (TCamara *obj, float a, float b );
TColision mover (TCamara *obj, float front, float side);
void cambiarAltura (TCamara *obj, float nueva_altura);
void iniVista ( TCamara *obj, float a, float b );
void iniVistav ( TCamara *obj, float f[2] );
void iniPosicion ( TCamara *obj, float x, float y, float z );
void iniPosicionv ( TCamara *obj, float f[3] );
void ponerVista (TCamara *obj);

#endif
