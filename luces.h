

typedef struct _LightInfo {
  GLfloat xyz[4];
  GLfloat *rgb;
  int enable;
} LightInfo;
